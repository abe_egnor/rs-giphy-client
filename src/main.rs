//use std::collections::HashMap;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let client = reqwest::ClientBuilder::new()
        .proxy(reqwest::Proxy::https("https://localhost:8080")?)
        .build()?;

    let resp = client.get("https://api.giphy.com/v1/gifs/trending?api_key=QO5Q33fonhqsXzM7XtkGsQRSPJWxsF9w&limit=1").send()
        .await?
        .text()
  //      .json::<HashMap<String, String>>()
        .await?;
    println!("{:#?}", resp);
    Ok(())
}